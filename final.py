import os
#os.environ['PYTHONHASHSEED'] = '0'

import random as rn
#rn.seed(0)
import numpy as np
import warnings
warnings.filterwarnings("ignore")

import json
import sys
import re
from time import sleep, time
import subprocess
import pandas as pd
from sklearn.svm import OneClassSVM as ocsvm
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score, accuracy_score
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
from joblib import dump, load
import seaborn as sns

threshold_rssi = 8
porcentagem_treino = 0.4

X = {
	"SSID"	:"",
	"MAC_AP":"",
	"E_REDE":"",
	"IP_AP"	:"",
	"RTT":"",
	"SEGUNDO_TTL":"",
	"RSSI":""
}

def coleta_dados():

	os.system("iwgetid -r > ssid.txt")
	os.system("ip -j route > ip.json")
	os.system("ip -j neigh > mac_gateway.json")
	os.system("iwconfig wlp2s0| grep \"Signal\" -B0 > rssi.txt")
	

	ip = json.load(open('ip.json'))
	mac = json.load(open('mac_gateway.json'))
	nome_ssid = open('ssid.txt', 'r').readline()

	nome_ssid = nome_ssid[:-1]

	ip_gateway = ip[0]["gateway"]
	endereco_rede = ip[len(ip)-1]["dst"]

	try:
		for x in mac:
			if x["dst"]==ip_gateway:
				mac_gateway = x["lladdr"]

	except KeyError:
		mac_gateway = ""

	
	rssi = open('rssi.txt', 'r').readlines()[-1].split("=")[-1].split(" ")[0]

	os.system("ping -qc 1 "+ip_gateway+" > ping.txt")
	os.system("traceroute 8.8.8.8 -f 2 -m 2 > traceroute.txt")
	
	try:

		ping = open('ping.txt', 'r').readlines()[-1].split("/")[-2]
		ttl = re.split("\(|\)",open('traceroute.txt', 'r').readlines()[-1])[1]
	
	except IndexError:
		ping = X.get("MEDIA_PING")
		ttl = X.get("SEGUNDO_TTL")


	return mac_gateway, ip_gateway, endereco_rede, nome_ssid, ping, ttl, rssi


def confere(mac_gateway, ip_gateway, endereco_rede, ping, ttl, rssi):
	
	diff = []

	diff.append("CERQUEIRA")
	if mac_gateway==X.get("MAC_AP"):
		diff.append(1)
	else:
		diff.append(0)

	if ip_gateway==X.get("IP_AP"):
		diff.append(1)
	else:
		diff.append(0)

	if endereco_rede==X.get("E_REDE"):
		diff.append(1)
	else:
		diff.append(0)

	try:
		rtt = '%.3f'%(float(X.get("RTT"))*0.9 + float(ping)*0.1)
	except TypeError:
		rtt = X["RTT"]

	diff.append(rtt)

	if ttl == X.get("SEGUNDO_TTL"):
		diff.append(1)
	else:
		diff.append(0)

	diff.append(rssi)

	#Se o RSSI melhorar mais que o threshold em uma iteração, possível ataque
	if (int(rssi) - int(X.get("RSSI"))) >= threshold_rssi:
		diff.append(0)
	else:
		diff.append(1)

	X["RSSI"] = rssi
	X["RTT"] = rtt

	return diff

def cria_dados_treinamento(t, db):
	tempo_inicio = time()

	dados = db

	dados['VAR_RSSI'] = []

	#print(dados)
	while time()-tempo_inicio < t:
		try:
			mac, ip, eRede, ssid, ping, ttl, rssi = coleta_dados()
			dif = confere(mac, ip, eRede, ping, ttl, rssi)
			print(dif)
			dados = pd.concat([dados, pd.DataFrame(dif, index=dados.columns).T]).reset_index(drop=True)

		except IndexError:
			continue

		sleep(1)

	s='\n'
	for i in X.values():
		s = s+" "+i
	s=s+'\n'
	redes = open('REDES.txt', 'a')
	redes.write(s)
	redes.close()

	return dados

def copia_X(ssid, mac, ip, er, ping, ttl, rssi):
	X["SSID"] = ssid
	X["MAC_AP"] = mac
	X["IP_AP"] = ip
	X["E_REDE"] = er
	X["RTT"] = ping
	X["SEGUNDO_TTL"] = ttl
	X["RSSI"] = rssi

def plot_perfil(dados):

	copia_dados = dados

	copia_dados.pop('RTT')
	copia_dados.pop('RSSI')
	col = {'0':0,'1':0, '2':0, '3':0, '4':0, '5':0}

	n = len(dados)

	for i in range (n):
		count = 5 - copia_dados.iloc[[i]].values.sum()
		col[str('%.0f'%count)] = col[str('%.0f'%count)] + 1

	print(col)
	sns.set_theme(style="ticks")
	hbar = sns.barplot(list(col.keys()), list(col.values()))
	hbar.bar_label(hbar.containers[0])
	plt.xlabel("Quantidade de características da conexão variadas")
	plt.ylabel("Quantidade de entradas de treino")
	plt.show()

def plot_seaborn(treino, teste, anomalia):
	copia_treino = pd.DataFrame(data=treino.values, columns=treino.columns)
	copia_teste_n = pd.DataFrame(data=teste.values, columns=treino.columns)
	copia_teste_a = pd.DataFrame(data=anomalia.values, columns=treino.columns)

	pontos = pd.concat([copia_treino, copia_teste_n, copia_teste_a]).reset_index(drop=True)

	x_2d = TSNE(n_components=2, init='pca', learning_rate='auto', random_state=0).fit_transform(pontos.values)

	j=[]
	for i in range (len(pontos)):
		if i<len(copia_treino):
			j.append("Dados de treinamento")
		elif i< (len(treino)+len(teste)):
			j.append("Dados normais para teste")
		else:
			j.append("Anomalias")
	
	pontos = pd.DataFrame(x_2d)
	pontos['Label'] = j
	
	fig, axis = plt.subplots(2,2, sharex=True, figsize=(10, 8))
	(ax1, ax2), (ax3, ax4) = axis

	axis = [ax1, ax2, ax3, ax4]

	clfLinear = ocsvm(nu=.05, kernel='linear', verbose=False)
	clfPoly = ocsvm(nu=.05, kernel='poly', gamma='scale', degree=5, coef0=-10, verbose=False)
	clfRBF = ocsvm(nu=.0003, kernel='rbf', gamma='auto', verbose=False)
	clfSigmoid = ocsvm(nu=.05, kernel='sigmoid', gamma='scale', coef0=-4.0, verbose=False)
	
	lista_clf = [clfLinear, clfPoly, clfSigmoid, clfRBF]

	for k in range (len(lista_clf)):

		clf = lista_clf[k]

		clf.fit(x_2d[:(len(copia_treino))])

		xx, yy = np.meshgrid(np.linspace(x_2d[:, 0].min()-5, x_2d[:, 0].max()+5, 500),
		                     np.linspace(x_2d[:, 1].min()-5, x_2d[:, 1].max()+5, 500))
		Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
		Z = Z.reshape(xx.shape)

		# Configuração do estilo Seaborn
		sns.set_theme(palette='dark')	

		axis[k].set_title("%s"%clf.get_params().get('kernel'))

		# Desenhar regiões de decisão e margens
		axis[k].contourf(xx, yy, Z, levels=np.linspace(Z.min(), 0, 7), cmap=plt.cm.PuBu)
		axis[k].contour(xx, yy, Z, levels=[0], linewidths=2, colors="darkred")
		axis[k].contourf(xx, yy, Z, levels=[0, Z.max()], colors="palevioletred")
		axis[k].set_xlabel(" ")
		axis[k].set_ylabel(" ")
		a = sns.scatterplot(data=pontos,x=0, y=1,
		                    hue="Label", palette=["green", "orange", "red"], edgecolor='black', ax=axis[k])
		axis[k].get_legend().remove()

	lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
	lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
	unique_labels = set(labels)
	legend_dict = dict(zip(labels, lines))
	unique_lines = [legend_dict[x] for x in unique_labels]
	fig.legend(unique_lines, unique_labels, scatterpoints=1, loc=4)
	plt.show()

def normalizar(dados_treino, ssid):

	normalizarRTT = MinMaxScaler()
	normalizarRTT.fit(np.array(dados_treino["RTT"]).reshape(-1,1))
	dump(normalizarRTT, '%srtt.txt'%ssid)

	normalizarRSSI = MinMaxScaler()
	normalizarRSSI.fit(np.array(dados_treino["RSSI"]).reshape(-1,1))
	dump(normalizarRSSI, '%srssi.txt'%ssid)
	
	rtt = normalizarRTT.transform(np.array(dados_treino["RTT"]).reshape(-1,1))
	rssi = normalizarRSSI.transform(np.array(dados_treino["RSSI"]).reshape(-1,1))

	return rtt, rssi

def simulacoes(porcentagem_treino):


	dados = pd.read_csv("dados_treino1.txt", sep="\s+", header=0)
	dados_treino, dados_teste_real = train_test_split(dados, train_size=porcentagem_treino, shuffle=False)

	dados_teste_anomalia = pd.read_csv("anomaliasNew2.txt", sep = "\s+", header=0)

	normalizarRTT = MinMaxScaler()
	dados_treino["RTT"] = normalizarRTT.fit_transform(np.array(dados_treino["RTT"]).reshape(-1,1))
	normalizarRSSI = MinMaxScaler()
	dados_treino["RSSI"] = normalizarRSSI.fit_transform(np.array(dados_treino["RSSI"]).reshape(-1,1))

	dados_teste_real["RTT"] = normalizarRTT.transform(np.array(dados_teste_real["RTT"]).reshape(-1,1))
	dados_teste_real["RSSI"] = normalizarRSSI.transform(np.array(dados_teste_real["RSSI"]).reshape(-1,1))

	dados_teste_anomalia["RTT"] = normalizarRTT.transform(np.array(dados_teste_anomalia["RTT"]).reshape(-1,1))
	dados_teste_anomalia["RSSI"] = normalizarRSSI.transform(np.array(dados_teste_anomalia["RSSI"]).reshape(-1,1))


	resultados = pd.DataFrame()

	resultados["dataset"] = ["Especificidade", "Sensibilidade", "Acurácia", "F1 Score"]

	y_treino = [1] * len(dados_treino)
	y_teste_real = [1] * len(dados_teste_real)
	y_teste_anomalias = [-1] * len(dados_teste_anomalia)

	clfLinear = ocsvm(nu=.05, kernel='linear', verbose=False).fit(dados_treino.values)
	clfPoly = ocsvm(nu=.05, kernel='poly', gamma='scale', degree=5, coef0=-10, verbose=False).fit(dados_treino.values)
	clfRBF = ocsvm(nu=0.0003, kernel='rbf', gamma='auto', verbose=False).fit(dados_treino.values)
	clfSigmoid = ocsvm(nu=.05, kernel='sigmoid', gamma='scale', coef0=-4, verbose=False).fit(dados_treino.values)
	
	lista_clf = [clfLinear, clfPoly, clfSigmoid, clfRBF]

	for clf in lista_clf:
		kernel = clf.get_params().get('kernel')

		pred_treino = clf.predict(dados_treino.values)
		pred_teste_real = clf.predict(dados_teste_real.values)
		pred_teste_anomalia = clf.predict(dados_teste_anomalia.values)
		
		sensibilidade = accuracy_score(y_teste_anomalias, pred_teste_anomalia)
		especificidade =  accuracy_score(y_teste_real, pred_teste_real)

		y_true = np.concatenate((y_teste_real, y_teste_anomalias))
		y_pred = np.concatenate((pred_teste_real, pred_teste_anomalia))
		f1 = f1_score(y_true, y_pred)

		t_anomalia = len(y_teste_anomalias)
		t_legitimo = len(y_teste_real)

		acuracia = ((sensibilidade*t_anomalia)+(especificidade*t_legitimo))/(t_legitimo+t_anomalia)

		resultados[kernel] = [especificidade, sensibilidade, acuracia, f1]

	print(resultados)
	#plot_seaborn(dados_treino, dados_teste_real, dados_teste_anomalia)


def main():

	#simulacoes(porcentagem_treino)
	#sys.exit()

	mac, ip, eRede, ssid, ping, ttl, rssi = coleta_dados()

	#Lê os SSIDs já registrados
	try:
		db = pd.read_csv("REDES.txt", sep = " ", header=0)

	except FileNotFoundError:
		redes = open('REDES.txt', 'w+')
		redes.write(str('SSID MAC ER/MASC IP RTT TTL RSSI'))
		redes.close()
		db = pd.read_csv("REDES.txt", sep = " ", header=0)

	tabela = db.loc[db['SSID']==ssid]

	#Se esse SSID já estiver no arquivo de entrada, recuperar os valores e compara
	if(tabela.index.size > 0):
		copia_X(tabela['SSID'].values,tabela['MAC'].values,tabela['IP'].values,
			tabela['ER/MASC'].values,tabela['RTT'].values,tabela['TTL'].values, tabela['RSSI'].values)
		#Recupera o OCSVM
		clf = load('%sclf.txt'%ssid)
		print("Dados recuperados")
	else:
		copia_X(ssid, mac, ip, eRede, ping, ttl, rssi)
		print("Primeira vez na rede\nIniciando coleta de dados da rede...")
		dados_treino = cria_dados_treinamento(100, db)
		print("Coleta finalizada. Treinando modelo...")

		dados_treino.pop("SSID")
		dados_treino['RTT'], dados_treino['RSSI'] = normalizar(dados_treino, ssid)

		clf = ocsvm(nu=0.0003, kernel='rbf', gamma='auto', verbose=False).fit(dados_treino.values)
		#Salva o OCSVM
		dump(clf, '%sclf.txt'%ssid)

	norm_RTT = load('%srtt.txt'%ssid)
	norm_RSSI = load('%srssi.txt'%ssid)

	while True:

		mac, ip, eRede, ssid, ping, ttl, rssi = coleta_dados()
		atual = np.array(confere(mac, ip, eRede, ping, ttl, rssi)[1:], dtype=np.float64).reshape(1,-1)
		#Normaliza RTT e RSSI
		atual[0][3] = norm_RTT.transform(atual[0][3].reshape(1,-1))
		atual[0][5] = norm_RSSI.transform(atual[0][5].reshape(1,-1)) 
		
		predSVM = clf.predict(atual)
		print(atual)
		print("OCSVM:",predSVM)


		sleep(2)

if __name__ == '__main__':
	main()